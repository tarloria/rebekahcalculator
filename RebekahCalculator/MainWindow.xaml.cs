﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RebekahCalculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public string Num1;
        public string Num2;
        public string Operator;
        //public double Answer;

        public int State = 0;

        static string Shift(string NumA, string NumB)
        {
            NumA = NumA + NumB;
            return NumA;
        }


        private void Button0Click(object sender, RoutedEventArgs e)
        {
            if (State == 0)
            {
                Num1 = Shift(Num1, "0");
                TextBlockNum1.Text = Num1;
            }
            if (State == 1)
            {
                Num2 = Shift(Num2, "0");
                TextBlockNum2.Text = Num2;
            }
        }

        private void Button1Click(object sender, RoutedEventArgs e)
        {
            if (State == 0)
            {
                Num1 = Shift(Num1, "1");
                TextBlockNum1.Text = Num1;
            }
            if (State == 1)
            {
                Num2 = Shift(Num2, "1");
                TextBlockNum2.Text = Num2;
            }
        }

        private void Button2Click(object sender, RoutedEventArgs e)
        {
            if (State == 0)
            {
                Num1 = Shift(Num1, "2");
                TextBlockNum1.Text = Num1;
            }
            if (State == 1)
            {
                Num2 = Shift(Num2, "2");
                TextBlockNum2.Text = Num2;
            }
        }

        private void Button3Click(object sender, RoutedEventArgs e)
        {
            if (State == 0)
            {
                Num1 = Shift(Num1, "3");
                TextBlockNum1.Text = Num1;
            }
            if (State == 1)
            {
                Num2 = Shift(Num2, "3");
                TextBlockNum2.Text = Num2;
            }
        }

        private void Button4Click(object sender, RoutedEventArgs e)
        {
            if (State == 0)
            {
                Num1 = Shift(Num1, "4");
                TextBlockNum1.Text = Num1;
            }
            if (State == 1)
            {
                Num2 = Shift(Num2, "4");
                TextBlockNum2.Text = Num2;
            }
        }

        private void Button5Click(object sender, RoutedEventArgs e)
        {
            if (State == 0)
            {
                Num1 = Shift(Num1, "5");
                TextBlockNum1.Text = Num1;
            }
            if (State == 1)
            {
                Num2 = Shift(Num2, "5");
                TextBlockNum2.Text = Num2;
            }
        }

        private void Button6Click(object sender, RoutedEventArgs e)
        {
            if (State == 0)
            {
                Num1 = Shift(Num1, "6");
                TextBlockNum1.Text = Num1;
            }
            if (State == 1)
            {
                Num2 = Shift(Num2, "6");
                TextBlockNum2.Text = Num2;
            }
        }

        private void Button7Click(object sender, RoutedEventArgs e)
        {
            if (State == 0)
            {
                Num1 = Shift(Num1, "7");
                TextBlockNum1.Text = Num1;
            }
            if (State == 1)
            {
                Num2 = Shift(Num2, "7");
                TextBlockNum2.Text = Num2;
            }
        }

        private void Button8Click(object sender, RoutedEventArgs e)
        {
            if (State == 0)
            {
                Num1 = Shift(Num1, "8");
                TextBlockNum1.Text = Num1;
            }
            if (State == 1)
            {
                Num2 = Shift(Num2, "8");
                TextBlockNum2.Text = Num2;
            }
        }

        private void Button9Click(object sender, RoutedEventArgs e)
        {
            if (State == 0)
            {
                Num1 = Shift(Num1, "9");
                TextBlockNum1.Text = Num1;
            }
            if (State == 1)
            {
                Num2 = Shift(Num2, "9");
                TextBlockNum2.Text = Num2;
            }
        }

        private void ButtonPeriodClick(object sender, RoutedEventArgs e)
        {
            if (State == 0)
            {
                Num1 = Shift(Num1, ".");
            }
            if (State == 1)
            {
                Num2 = Shift(Num2, ".");
            }
        }

        private void ButtonPlusClick(object sender, RoutedEventArgs e)
        {
            Operator = "+";
            State = 1;
            TextBlockOperator.Text = "+";
        }

        private void ButtonMinusClick(object sender, RoutedEventArgs e)
        {
            Operator = "-";
            State = 1;
            TextBlockOperator.Text = "-";
        }

        private void ButtonMultiplyClick(object sender, RoutedEventArgs e)
        {
            Operator = "*";
            State = 1;
            TextBlockOperator.Text = "*";
        }

        private void ButtonDivideClick(object sender, RoutedEventArgs e)
        {
            Operator = "/";
            State = 1;
            TextBlockOperator.Text = "/";
        }

        private void ButtonEqualsClick(object sender, RoutedEventArgs e)
        {
            if (double.TryParse(Num1, out double Num1Dbl))
            {
                if (double.TryParse(Num2, out double Num2Dbl))
                {
                    if (Operator == "+")
                    {
                        TextBlockAnswer.Text = (Num1Dbl + Num2Dbl).ToString();
                    }
                    else if (Operator == "-")
                    {
                        TextBlockAnswer.Text = (Num1Dbl - Num2Dbl).ToString();
                    }
                    else if (Operator == "*")
                    {
                        TextBlockAnswer.Text = (Num1Dbl * Num2Dbl).ToString();
                    }
                    else if (Operator == "/")
                    {
                        TextBlockAnswer.Text = (Num1Dbl / Num2Dbl).ToString();
                    }
                }
            }
        }

        private void ButtonResetClick(object sender, RoutedEventArgs e)
        {
            Num1 = "";
            Num2 = "";
            State = 0;

            TextBlockNum1.Text = "";
            TextBlockNum2.Text = "";
            TextBlockOperator.Text = "";
            TextBlockAnswer.Text = "";

        }
    }
}